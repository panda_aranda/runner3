﻿using System.Collections.Generic;
using UnityEngine;

public class Controller_Instantiator : MonoBehaviour
{
    public List<GameObject> enemies;
    public List<GameObject> nafta;
    public GameObject instantiatePos;
    public float respawningTimer;
    private float time = 0;

    void Start()
    {
        Controller_Enemy.enemyVelocity = 2;
        Controller_Nafta.naftaVelocity = 2;
    }

    void Update()
    {
        SpawnEnemies();
        ChangeVelocity();
        SpawnNafta();
    }

    private void ChangeVelocity()
    {
        time += Time.deltaTime;
        Controller_Enemy.enemyVelocity = Mathf.SmoothStep(0.5f, 15f, time / 45f);
        Controller_Nafta.naftaVelocity = Mathf.SmoothStep(0.5f, 15f, time / 45f);
    }

    private void SpawnEnemies()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(enemies[UnityEngine.Random.Range(0, enemies.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(1, 6);
        }
    }
    private void SpawnNafta()
    {
        respawningTimer -= Time.deltaTime;

        if (respawningTimer <= 0)
        {
            Instantiate(nafta[UnityEngine.Random.Range(0, nafta.Count)], instantiatePos.transform);
            respawningTimer = UnityEngine.Random.Range(1, 6);
        }
    }
}

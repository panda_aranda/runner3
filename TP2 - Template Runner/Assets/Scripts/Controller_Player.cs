﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Player : MonoBehaviour
{
    public Slider vida;
    private Rigidbody rb;
    public float jumpForce = 10;
    private float initialSize;
    private int i = 0;
    private bool floored;
    public int hpMAX = 100;
    public int hpActual;
    public GameObject asesinarEffectPrefab;




    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        initialSize = rb.transform.localScale.y;
        hpActual = hpMAX;
    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            hpActual -= 25;

        }

        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = true;

        }
    }
    private void Update()
    {
        GetInput();
        Muerte();
        vida.value = hpActual / hpMAX;
    }

    private void GetInput()
    {
        Jump();
        Duck();

    }

    private void Jump()
    {
        if (floored)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                rb.AddForce(new Vector3(0, jumpForce, 0), ForceMode.Impulse);
            }
        }
    }

    private void Duck()
    {
        if (floored)
        {
            if (Input.GetKey(KeyCode.S))
            {
                if (i == 0)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, rb.transform.localScale.y / 2, rb.transform.localScale.z);
                    i++;
                }
            }
            else
            {
                if (rb.transform.localScale.y != initialSize)
                {
                    rb.transform.localScale = new Vector3(rb.transform.localScale.x, initialSize, rb.transform.localScale.z);
                    i = 0;
                }
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.S))
            {
                rb.AddForce(new Vector3(0, -jumpForce, 0), ForceMode.Impulse); }
        }
    }


    

    public void Muerte()
    {
        if (hpActual==0)
        {
            Destroy(this.gameObject);
            Controller_Hud.gameOver = true;
        }
        
    }
    

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Floor"))
        {
            floored = false;
           
        }
    }

}

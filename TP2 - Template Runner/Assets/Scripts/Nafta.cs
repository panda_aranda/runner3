using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nafta : MonoBehaviour
{
    public float extraTime = 5f; 


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            BarraProgreso BarraProgreso = other.GetComponent<BarraProgreso>();

            if (BarraProgreso != null)
            {
                BarraProgreso.AddTime(extraTime);
            }

            Destroy(gameObject); 

        }
    }

}
